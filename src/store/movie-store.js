import {defineStore} from "pinia";

export const movieStore = defineStore('resource', {
  state: () => ({
    movieList: []
  }),
  actions: {
    addItem(resource) {
      if (this.movieList.find(movie => movie.id === resource.id)) {
        let index = this.movieList.findIndex(item => item.id === resource.id);
          this.movieList[index].title = resource.title
          this.movieList[index].director = resource.director
          this.movieList[index].summary = resource.summary
          this.movieList[index].genres = resource.genres
      } else {
        this.movieList.push(resource);
      }
    },

    deleteItem(id) {
      let index = this.movieList.findIndex(item => item.id === id);
      this.movieList.splice(index, 1);
    }
  }
})
