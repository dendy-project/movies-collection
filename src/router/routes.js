
const routes = [
  {
    path: '/',
    component: () => import('pages/movies-list/MoviesListPage.vue'),
  },
  {
    path: '/create',
    component: () => import('pages/movies-detail/MoviesDetailPage.vue'),
  },
  {
    path: '/detail/:id',
    component: () => import('pages/movies-detail/MoviesDetailPage.vue'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
